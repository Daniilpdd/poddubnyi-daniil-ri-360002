var express = require('express');
var router = express.Router();
const { UserSignUp,GetUserInfo,UserLoginUp,UserUpdate,IsLoggedToday } = require('../workers/user-workers');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/sign-up',UserSignUp);
router.post('/update',UserUpdate);
router.post('/is-logged',IsLoggedToday);
router.post('/log-in',UserLoginUp);
router.post('/get-user-info',GetUserInfo);

module.exports = router;
