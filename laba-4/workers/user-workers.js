const User = require('../models/user');

module.exports = {
    UserSignUp,
    GetUserInfo,
    UserLoginUp,
    UserUpdate,
    IsLoggedToday
};

function UserSignUp(req,res,next) {
    let { login, pass, fn, mn, ln, birthDate } = req.body;
    if(!login) res.json(new Error('Login is needed'));
    User.findOne({login: login},(err,user) =>  {
        if(err) res.json(new Error(err));
        if(user) {
            res.json(new Error('Login is used'));
        } else {
            let newUser = new User();
            newUser.login = login;
            newUser.pass = pass;
            newUser.fn = fn;
            newUser.mn = mn;
            newUser.ln = ln;
            newUser.birthDate = birthDate;
            newUser.lastLogin = "";

            newUser.save((err,doc) => {
                if(err) res.json(new Error(err));
                res.json(doc);
            });
        }
    });
}

function GetUserInfo(req,res,next) {
    let { userID } = req.body;
    if(!userID) res.json(new Error('LOH'));
    User.findById(userID, (err,user) => {
        if(err) res.json(new Error(err));
        res.json(user);
    });
}

function UserLoginUp(req,res,next) {
    let { login, pass } = req.body;
    if(!login) res.json(new Error('Login is needed'));
    if(!pass) res.json(new Error('Password is needed'));
    User.findOne({login: login},(err,user) => {
        if(err) res.json(new Error('Login not found'));
        if(user){
            if(pass != user.pass) res.json(new Error('pass not worked'));
            else {
                let today = new Date();
                user.lastLogin = `${today.getDate()}.${(today.getMonth() + 1)}.${today.getFullYear()}`;
                user.save((err,doc) => {
                    if(err) res.json(new Error(err));
                    res.json(doc);
                });
            }
        }
    });
}

function UserUpdate(req, res, next){
    let { login, pass, fn, mn, ln, birthDate } = req.body;
    if(!login) res.json(new Error('Login is needed'));
    User.findOne({login: login},(err,user) =>  {
        if(err) res.json(new Error(err));
        if(!user) {
            res.json(new Error('Login not found'));
        } else {
            if(user.login) user.login = login;
            if(user.pass) user.pass = pass;
            if(user.fn) user.fn = fn;
            if(user.mn) user.mn = mn;
            if(user.ln) user.ln = ln;
            if(user.birthDate) user.birthDate = birthDate;

            user.save((err,doc) => {
                if(err) res.json(new Error(err));
                res.json(doc);
            });
        }
    });
}

function IsLoggedToday(req, res, next) {
    let { login } = req.body;
    if (!login) return res.json(new Error('Login is needed'));
    User.findOne({login:login}, (err, user) => {
        if(err) return res.json(new Error(err));
        if(!user) return res.json(new Error('Login not found'));
        if(!user.lastLogin) return res.json(new Error('User not login'));
        else {
            let today = new Date();
            return res.json(user.lastLogin === `${today.getDate()}.${(today.getMonth() + 1)}.${today.getFullYear()}`);
        }
    });
}