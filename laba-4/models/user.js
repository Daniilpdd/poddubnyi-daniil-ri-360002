const mongoose = require('mongoose');

let Schema = mongoose.Schema({
    login:{
        type: String,
        unique: true
    },
    pass:{
        type: String
    },
    fn:{
        type: String
    },
    mn:{
        type: String
    },
    ln:{
        type: String
    },
    birthDate:{
        type: String
    },
    lastLogin:{
        type: String
    }
},{
    versionKey: false
});
module.exports = mongoose.model('User', Schema);